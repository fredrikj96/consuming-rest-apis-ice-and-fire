package com.example.task_ice_and_fire.data;

import org.json.JSONObject;
import org.springframework.stereotype.Service;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;


@Service
public class IceAndFireRepository {


    public static String getHouseTaragyen() throws IOException {
        try {
            URL url = new URL("https://anapioficeandfire.com/api/houses/378");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");


            JSONObject houseObject = null;
            if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                InputStreamReader reader = new InputStreamReader(connection.getInputStream());
                BufferedReader bufferedReader = new BufferedReader(reader);
                String line;
                StringBuffer responseContent = new StringBuffer();

                while ((line = bufferedReader.readLine()) != null) {
                    responseContent.append(line);
                }
                bufferedReader.close();
                houseObject = new JSONObject(responseContent.toString());
            }

            return houseObject.getJSONArray("titles").toString();

        } catch (Exception e) {
            return e.getMessage();
        }

    }

}
