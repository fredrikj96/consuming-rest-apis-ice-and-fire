package com.example.task_ice_and_fire;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TaskIceAndFireApplication {

    public static void main(String[] args) {
        SpringApplication.run(TaskIceAndFireApplication.class, args);
    }

}
