package com.example.task_ice_and_fire.controller;

import com.example.task_ice_and_fire.data.IceAndFireRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;


@RestController
public class IceAndFireController {

@Autowired
private IceAndFireRepository iceAndFireRepository;

@GetMapping("/titles")
    public ResponseEntity <String> getHouseTaragyen () throws IOException {

    return new ResponseEntity<>(iceAndFireRepository.getHouseTaragyen(), HttpStatus.OK);
}

}
